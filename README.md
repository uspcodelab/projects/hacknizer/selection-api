# apollo-example
An simple apollo server using koa adapter and sequelize, and jest for unit testing.

## Apollo Server
Apollo server is a GraphQL API generator with a series of helpers that speed up the development,
like an SDL to define types, queries and mutations. It also takes care of the HTTP requests.

## Koa Adapter
Koa is a extremelly small Node.JS framework used to create APIs. Koa only takes care of the routing,
which is everythin we need, Apollo Server will take care of parsing the requests, passing them to the
proper resolver.

## Sequelize
Sequelize is an ORM (Object Relational Mapper), which means it generates SQL for us. This enable us
to write JavaScript instead of pure SQL.

## Jest
Jest is a test framework made by Facebook, it's very good and easy to learn.

---

## How to get started?
Build the docker image by running:
```
docker-compose build --no-cache --force-rm
```

And start it with:
```
docker-compose up
```

---

## What are all these files?
* `server.js` holds the configuration for bot Koa and Apollo Server.

* `db/` holds all sequelize files.
  * `config.js` holds all configurations for the Sequelize instance.
  * `models/` holds all the files with our defined models.
  * `seeds/` holds all the files with our seeds defintions

* `graphql/` holds all graphql files
  * `schema.js` holds all our types definitions, combine these definitions with queries and mutations.
     And exports this combinations and the resolvers.
  * `queries.js` holds all our queries definitions.
  * `mutations.js` holds all our mutations definitions.
  * `resolvers/`: holds all our resolver files. Resolvers are the function that is called when a
     query or mutation is called
