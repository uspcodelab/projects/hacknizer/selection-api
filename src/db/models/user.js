import sequelize from "sequelize";
import db from "../config.js";

const User = db.define('user', {
  name: { type: sequelize.STRING },
});

export default User;
