import Sequelize from 'sequelize';

export default new Sequelize('test', 'test', 'test', {
  host: 'db',
  dialect: 'postgres',
  operatorAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  define: {
    timestamps: false
  }
});
